package com.ivl.car.domain;

import lombok.Data;

@Data
public class CloudConfigurationFile {
	
	private String API1SEARCHURL;
    private String API1BOOKURL;
	private String API2SEARCHURL;
	private String API2BOOKURL;
	private String token;
	private String sucMsg;
	private String failMsg;
	private String noCarMsg;

}
