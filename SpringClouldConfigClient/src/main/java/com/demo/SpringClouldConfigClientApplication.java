package com.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringClouldConfigClientApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringClouldConfigClientApplication.class, args);
	}

}
