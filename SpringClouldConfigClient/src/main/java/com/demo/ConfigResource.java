package com.demo;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RefreshScope
public class ConfigResource {


	@Value("${msg}")
	private String message;
	
	@Value("${msg1}")
	private String message1;
	
	
	@RequestMapping("/config")
	public String getConfigValues() {
		return message+"   "+message1;
	}

}
